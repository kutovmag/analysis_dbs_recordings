% Does the bootstrap - generate random indexes of the singal with replecament
% with the function bootstrp and than for each bootstrap sample count the
% statistics - max volume under the difference of the mean spectogram of 
% the signals
%
%
% Arguments:
%  x - KxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
%  y - LxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
% real_maximum - statistic counted on the origin data
% B - number of bootstrap samples we want
%
% Returns:
% p_value - int, result of the bootstrap statistic


function [p_value] = bootstrap_statistic_spectrum(x, y, real_maximum, f_dec, B)
    maximum = zeros(1,B);
    len_x = length(x(:,1));
    z = [x; y];

    [~, bootsam] = bootstrp(B, [], z);
    
    %%
    for k = 1 : B
        % generate random numbers with replacement
        rand_x = z(bootsam(1:len_x,k),:); 
        rand_y = z(bootsam(len_x + 1: end,k),:);
        
        rand_x_spect_mean = spectrogram_mean(rand_x, f_dec);
        rand_y_spect_mean = spectrogram_mean(rand_y, f_dec);

        rand_diff = rand_y_spect_mean - rand_x_spect_mean;
        
        max_positive = max_volume(rand_diff, 1, 8, 24);
        max_negative = max_volume(rand_diff, -1, 8, 24);

        
        maximum(k) = max(max_positive, max_negative);
    end
    p_value = (sum(maximum >= real_maximum) + 1) / (B + 1);

end
