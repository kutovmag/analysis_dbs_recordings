% t-test of x and y in spectral domain with error rate 5% in all t-tests
%
% Arguments:
% X - KxN matrix of signals (recorded in individual channels); signals
%     come in K rows, samples in N columns
% Y - KxN matrix of signals (recorded in individual channels); signals
%     come in K rows, samples in N columns
% b - number of the done t-test - length of the Spect_x and Spect_y
% fs - sampling frequency
%
% Retuns: 
% p_value - array of the p_values of X and Y in spectral domain
% intervals - indexes of the test where the p_value is lesser than required
%              error - alpha

function [p_value, intervals] = interval_ttests_spectrum(X, Y, b, fs)
    spect_X = count_spectrum(X, fs);
    spect_Y = count_spectrum(Y, fs);
    
    % t-test of two matrixes 
    alpha = 0.05 / b; 
    
    % count p value and find it is smaller than alpha
    [~, p_value] = ttest2(spect_X, spect_Y);
    intervals = find(p_value < alpha);

end