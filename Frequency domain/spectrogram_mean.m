% Count the mean of the spectrograms from all of the signals in the matrix
% X with use of function get_spectrogram.m
%
% Arguments:
% X - KxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
% fs - sampling frequency
%
% Returns:
% spect_mean - 24x8 matrix (set default) of the mean from spectrograms from
%              all of the signals, the spectrum is just to 200Hz and the
%              size of the frequency intervals is modified into default
%              size in function get_spectrogram.m
% F_200 - array of origin distribution of frequnecies to 200Hz for the 
%         spectrogram 

function [spect_mean, F_200] = spectrogram_mean(X, fs)
    spect_mean = zeros(24,8);
    num_x = length(X(:,1));
    for i = 1 : num_x
        [S, F] = get_spectogram(X(i,:), fs);
        spect_mean = spect_mean + (S / num_x);
    end
    F_200 = F(F < 200);

end
