% return the maximum volume under the area y and on which indexes we can
% find it - can find just volume above null or under null (determines by 
% parameter sgn)
%
% the volume is counted in 4 directions - it has to be connected with the 
% other squeres of same sign on the left, right, top or bottom to be taken 
% into consideration  
%
% Arguments:
% y - KxN array - under/over area y the volume is counted
% sgn - signum - if we want to count the plus volume (1) or the minus
%       volume (-1)
% num_time - number of time intervals in the spectrum of y
% num_freq - number of frequency intervals in the spectrum of y
%
% Returns:
% max_area - return the maximum volume under the area
% max_interval - Mx2 array, return indexes of position of the maximum valume
%                first index denote row the second denote column
% 
% in both cases we look for positive values so in case we look for negative 
% volume we have to change the sign in all signal   

function [max_area, max_interval] = max_volume(y, sgn, num_time, num_freq)
    if sgn == -1
        y = -1 * y; 
    end
    max_interval = [];
    max_area = 0;
    sum_y = sum(sum(y));
    % if the sum is null the while cyklus would not run
    if sum_y == 0
        sum_y = 1;
    end

    while sum_y ~= 0  % which means that all states were discovered
        sgn_area = 0;
        sgn_interval = [];   
        indexes = [1, 1];
        while isempty(indexes) == 0
            % alwayes take the first index from the array
            row = indexes(1,1);
            col = indexes(1,2);
            if length(indexes(:,1)) > 1
                indexes = indexes(2:end,:);
            else
                indexes = [];
            end
           
            if sign(y(row, col)) == 1
                sgn_area = sgn_area + y(row, col);
                % we examined this state
                y(row,col) = 0;
                sgn_interval = [sgn_interval; row, col];
                
                % add new possible indexes for examination
                if row == 1
                    if col == 1
                        indexes = [indexes; row + 1, col; row, col + 1];
                    elseif col == num_time
                        indexes = [indexes; row + 1, col; row, col - 1];
                    else
                        indexes = [indexes; row + 1, col; row, col - 1; row, col + 1];
                    end
                elseif row == num_freq
                    if col == 1
                        indexes = [indexes; row - 1, col; row , col + 1];
                    elseif col == num_time
                        indexes = [indexes; row - 1, col; row, col - 1];
                    else
                        indexes = [indexes; row - 1, col; row, col + 1; row, col - 1];
                    end
                else
                    if col == 1
                        indexes = [indexes; row + 1, col; row - 1, col; row, col + 1];
                    elseif col == num_time
                        indexes = [indexes; row + 1, col; row - 1, col; row, col - 1];
                    else
                        indexes = [indexes; row + 1, col; row - 1, col; row, col + 1; row, col - 1];
                    end                    
                end    
            end
            % no interval was find, we moved to onether interval
            if sgn_area == 0
                if row < num_freq
                    row = row + 1;
                elseif row == num_freq && col < num_time
                    row = 1;
                    col = col + 1;
                else
                    sum_y = 0;
                    break
                end
                indexes = [row, col];
            end
        end
        if sgn_area > max_area
            max_area = sgn_area;
            max_interval = sgn_interval;
        end
    end


