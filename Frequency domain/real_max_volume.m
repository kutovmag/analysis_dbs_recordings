% for the spectrum it counts the maximum volume under the area x and also
% where it can be found
%
% Arguments:
%  x - KxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
% num_time - number of time intervals in the spectrogram
% num_freq - number of frequency intervals in the spectrum
% 
% Returns:
% maximum - maximum volume under the area x
% table_interval - num_freq x num_time matrix, indexes of the position 
%                  where the maximum volume can be found are denoted with 0
%                  othervise it is 1



function [maximum, table_interval] = real_max_volume(x, num_time, num_freq)
    
    [plus_area, plus_interval] = max_volume(x, 1, num_time, num_freq);
    [minus_area, minus_interval] = max_volume(x, -1, num_time, num_freq);
    
    if plus_area >= minus_area 
        maximum = plus_area;
        max_interval = plus_interval;
    else
        maximum = minus_area;
        max_interval = minus_interval;
        
    end
    
    table_interval = ones(num_freq, num_time);
    for j = 1 : length(max_interval(:,1))
        table_interval(max_interval(j,1), max_interval(j,2)) = 0; 
    end
end
