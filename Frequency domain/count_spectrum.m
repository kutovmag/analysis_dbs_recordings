% Count from the individual signals spectrograms and reshape it into
% required size (1x(24x8))
%
% Arguments
% x - KxN matrix of signals (recorded in individual channels); signals
%     come in K rows, samples in N columns
% fs - sampling frequency
%
% Returns: 
% M - 1 x 192, array, spectrograms of the signals from x which is reshaped 
%     into 1xL array (L = 192)

function M = count_spectrum(X, fs)
    nums = length(X(:,1));
    M = zeros(nums, 192);
    for i = 1 : nums
        [S, ~] = get_spectogram(X(i,:), fs);
        M(i,:) = reshape(S, 1, 192);
    end

end