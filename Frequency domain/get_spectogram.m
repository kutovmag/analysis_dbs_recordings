% return spectogram of the signal but in required size to the frequency
% 200Hz
% the size of the final spectrogram is set in the code
%
% Arguments:
% x - N - array of the signal, it should have enough size to create it 24x8
%         spectrogram
% fs - sampling frequency
%
% Returns:
% S_cut - 24x8 matrix, spectrum cut to the frequency 200Hz in required size
% F - array of all frequencies
%

function [S_cut, F] = get_spectogram(x, fs)
    ww = 0.2 * fs;
    nn = round(0.75 * ww); % overlap
    zz = 0.2 * ww;
    idx = 1 : ww - nn : length(x) - ww + 1; %index of the signal's beginning 
    win = hamming(ww); % hamming window
    S = zeros(ww + zz, length(idx));  
    N = ww + zz;
    
    for i = 1 : length(idx)
        xseg = x(idx(i):idx(i) + length(win) - 1); 
        xseg = xseg .* win(:)';      
        xseg = [xseg, zeros(1, zz)]; 

        X = fft(xseg); %spectrum
        PSD = (abs(X) .^ 2) / N; 
        S(:, i) = PSD;
    end
    
    F = linspace(0, fs - (fs / N), N);
    % spectrogram on required frequencies 
    % to the 50Hz same, to the 100Hz it is connection of two windows and
    % from 100 to 200Hz it is connection of 4 windows
    % so in the end from the previous 48 frequency windows it is 24
    freq_idx = [13 15 17 19 21 23 25 29 33 37 41 45 48];
    
    S_cut = zeros(24, 8);
    S_cut(1:12,:) = S(1:12,:);
    for j = 1 : length(freq_idx) - 1
        S_cut(j+12,:) = sum(S(freq_idx(j):freq_idx(j + 1) - 1,:));
    end


    % figure(5);
    % plot(t, y);
    % xlabel('time(s)'); ylabel('y[n]');

%     figure(1);
%     subplot(211);
%     imagesc(Ti, F, S); axis xy; % zobrazte spektrogram jako obrázek
%     c = colorbar; c.Label.String='Power'; % barevnou škálu s popiskem
%     xlabel('time(s)'); ylabel('f(Hz)');
%     title('ww = 2s');
% 
%     subplot(212);
%     imagesc(Ti, F, 10*log10(S)); axis xy; % v dB
%     c = colorbar; c.Label.String='dB';
%     xlabel('time(s)'); ylabel('f(Hz)');
    
%     figure(2);
%     imagesc(Ti, F, 10 * log10(S)); axis xy; 
%     c = colorbar; c.Label.String='dB';
%     xlabel('time(s)'); ylabel('f(Hz)'); 
%     ylim([0,200]);
    
end