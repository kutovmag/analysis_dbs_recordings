% return max area under the curve - x
%
% Arguments:
%  x - N-array of the signal
%
% Returns:
% maxArea - max area under the curve - x
% max_start - index of the beginnig of the max area
% max_end - index of the end of the max area


function [maxArea, max_start, max_end] = max_area(x)
    maxArea = 0;
    previous_mark = sign(x(1)); %sign of the first sample
    beginning = 1;
    max_start = 0;
    max_end = 0;
    for i = 1 : length(x)
        if i < length(x)
            mark = sign(x(i + 1));
        else
            mark = -1 * previous_mark;
        end
        %if the sample is zero it has sign of the previous sample
        if mark == 0
            mark = previous_mark;
        end
        
        %if the signal cross zero
        if mark ~= previous_mark
           % count area in the interval
           % area = abs(trapz(t(beginning : i), x(beginning : i)));
           area = abs(sum(x(beginning : i)));
           if area > maxArea
              maxArea = area;
              max_start = beginning;
              max_end = i;
           end
           beginning = i + 1;
        end
        previous_mark = mark;
    end
end
