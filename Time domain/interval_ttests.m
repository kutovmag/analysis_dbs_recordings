% t-test of x and y in time domain at the 5% significance level
%
% Arguments:
% X - KxN matrix of signals (recorded in individual channels); signals
%     come in K rows, samples in N columns
% Y - KxN matrix of signals (recorded in individual channels); signals
%     come in K rows, samples in N columns
% b - number of the done t-test - length of the avgX and avgY
%
% Returns: 
% p_value - array of the p_values of X and Y in time domain
% intervals - indexes of the test where the p_value is lesser than required
%              error - alpha



function [p_value, intervals] = interval_ttests(X, Y, b)
    avgX = average(X, b);
    avgY = average(Y, b);
    
    % t-test of two matrixes 
    alpha = 0.05 / b; 
    
    % count p value and find it is smaller than alpha
    [~, p_value] = ttest2(avgX, avgY);
    intervals = find(p_value < alpha);

end