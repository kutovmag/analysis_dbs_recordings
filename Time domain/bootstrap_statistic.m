% Do the bootstrap - generate random indexes of the singal with replecament
% with the function bootstrp and than for each bootstrap sample count the
% statistics - max area under the difference of the mean of the signals
%
%
% Arguments:
%  x - KxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
%  y - LxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
% real_maximum - statistic counted on the origin data
% B - number of bootstrap samples we want
%
% Returns:
% p_value - int, result of the bootstrap statistic
%

function [p_value] = bootstrap_statistic(x, y, real_maximum, B)
    maximum = zeros(1,B);
    len_x = length(x(:,1));

    z = [x; y];

    % vector of possible indexes in the matrix
    [~, bootsam] = bootstrp(B, [], z);
    
    %%
    for k = 1 : B
        % generate random numbers with replacement
        rand_x = z(bootsam(1:len_x,k),:); 
        rand_y = z(bootsam(len_x + 1: end,k),:);

        rand_x_mean = mean(rand_x);
        rand_y_mean = mean(rand_y);

        rand_diff = rand_y_mean - rand_x_mean;
        maximum(k) = max_area(rand_diff);
    end
    p_value = (sum(maximum >= real_maximum) + 1) / (B + 1);
end