% Cut the signal into the required number of time intervals and each
% intervals represented by the average number of the interval
%
%
% Arguments:
%  x - KxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
% nbrTimeIntervals - number of time intervals we want to create in the
%                    given signal
%
% Returns:
% avg - K x nbrTimeIntervals matrix same rows as the input matrix x and the
%       length of the signal is shorten into required number of time intervals
% 

function [avg] = average(x, nbrTimeIntervals)
    % number of inexes in time interval
    idx_interval = (floor(length(x(1,:)) / nbrTimeIntervals));
    avg = zeros(length(x(:,1)), nbrTimeIntervals);
    idx_beginning = 1;
    
    for i = 1 : nbrTimeIntervals 
        % average of every row of the matrix on the time interval
        avg(:, i) = mean(x(:, idx_beginning : idx_beginning + idx_interval - 1), 2);
        idx_beginning = idx_beginning + idx_interval;
    end
end
