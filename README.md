# Analysis_DBS_recordings
Software tools used for bachelor thesis "Analysis of deep brain stimulation recordingsfrom emotional task".
Developed for MATLAB R2020a.

In the given data, the signals were marked differently - MEx = macro electrode recordings (LFP), UExd = microelectrode recordings (MER).

Preprocessing - the folder Matlab functions for loading the data from .d files, filtering loaded data with the comb filter, and then dividing data into matrixes of the parts of the signal which occur 550 ms after the required emotion tag - H (happy), N (neutral), F (fearful). If any of the significant parts contained an artifact - that part was not used.

Removing action potentials contains Matlab functions for removing action potentials using algorithms WaveClus.

Time domain contains Matlab functions for statistical tests modified in the time domain. 

Frequency domain contains Matlab functions 
for statistical tests modified for the frequency domain.


STN.m is a Matlab file that runs both hypothesis testing (bootstrap and t-tests) for the LFP signal in STN in the time domain.

STN_spec.m is a Matlab file that runs both hypothesis testing (bootstrap and t-test) for the LFP signal in STN in the frequency domain.

STN_UExd.m is a Matlab file that runs both hypothesis testing (bootstrap and t-test) for the MER signal in STN in the time domain.

STN_UExd_spec.m is a Matlab file that runs both hypothesis testing (bootstrap and t-test) for the MER signal in STN in the frequency domain.
