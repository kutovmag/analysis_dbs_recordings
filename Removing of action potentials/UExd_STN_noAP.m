% Removed from the UExd(microelectrode recordings) signals actions potentials from all of the neurons
% and because the action potentials were computed from UExd signals without
% artifacts, it is modified to the original length of the signal so it can
% be processed same as the UExd signals with action potentials
%
% It used text files with information:
% 	list_modified2.txt - names of the .d files with the data
% 	fs.mat - the sampling frequency for data
% 	STN_neurons.mat - matrix, in the first column is the order of the 
%   recordings which were identified in STN, and in the second column is 
%   the number of the electrode which was active in STN (recorded in STN)
% 	UExd_artefact.txt - list of the .csv files with the beginning and 
%   the end of the time where the artifact occurs.
% 
% Data from .mat files are loaded (electrodes that were active in STN). 
% Data is already preprocessed - comb filter (Loading.mat)
% time_file - file where is saved the .mat files with the inormation about
%             action potentials - outcome of the WaveClus algorithm
% signal_file - file where are saved the signals in the .mat file with same
%               name as the name of the signal but in this .mat file 
%               the signal is saved in variable data and 
%               in variable sr is saved the sampling frequency
%
% save_file - path, where we want to save the results from bootstrap 
%             and t-tests.

file = 'E:/FEL/Bakalarka/matlab/list_modified2.txt';
path = 'E:/FEL/Bakalarka/preprocessed_data_origin/';

f = fopen(file);
fileNames = textscan(f, '%s');
fclose(f);

fileNames = fileNames{1};
originNames = fileNames;
for l = 1 : length(originNames)
   originNames{l} = originNames{l}(1:end-2); 
end 
fileNames = strcat(fileNames,'_preprocessed.mat');

file = 'E:/FEL/Bakalarka/mat_files/fs.mat';
load(file);

stn_file ='E:/FEL/Bakalarka/mat_files/STN_neurons.mat';
load(stn_file);

artefact_file = 'E:/FEL/Bakalarka/art_annot/';

file = 'E:/FEL/Bakalarka/matlab/UExd_artefact.txt';
f = fopen(file);
artefactNames = textscan(f, '%s');
fclose(f);
artefactNames{1} = strcat(artefactNames{1},'_art.csv');

nbr_STN = length(stn_neurons(:,1));

times_file = 'C:/Users/magda/Desktop/wave_clus-master/';
signal_file = 'C:/Users/magda/Desktop/wave_clus-master/data/';
save_file  = 'E:/FEL/Bakalarka/UExd_noAP/'; 

for k = 1 : nbr_STN   
	load(strcat(path,fileNames{stn_neurons(k,1)}));
    Uexd = x50(6:end,:);
    x50 = Uexd(stn_neurons(k,2),:);
    
    f = fopen(strcat(artefact_file, artefactNames{1}{stn_neurons(k,1)}));
    artefacts = textscan(f, '%s %s %s');
    fclose(f);
 
    artefact_channel = zeros(length(artefacts{1}),5);
    for p = 1 : length(artefacts{1})
        if sum(artefacts{3}{p} == '1') == 1
           artefact_channel(p,1) = 1; 
        end
        if sum(artefacts{3}{p} == '2') == 1
           artefact_channel(p,2) = 1; 
        end
        if sum(artefacts{3}{p} == '3') == 1
           artefact_channel(p,3) = 1; 
        end
        if sum(artefacts{3}{p} == '4') == 1
           artefact_channel(p,4) = 1; 
        end
        if sum(artefacts{3}{p} == '5') == 1
           artefact_channel(p,5) = 1; 
        end
        if sum(artefacts{3}{p} == '0') == 1
           artefact_channel(p,:) = 1; 
        end

    end
    
    artefact = zeros(length(artefacts{1}),2);
    for j = 1 : length(artefacts{1})
       artefact(j,1) = str2num(artefacts{1}{j}(1:end-1));
       artefact(j,2) = str2num(artefacts{2}{j}(1:end-1));
    end
    
    artefact_idx = artefact .* fs;
    artefact_UExd = artefact_idx(artefact_channel(:, stn_neurons(k,2)) == 1,:);
    signal = load(strcat(signal_file,originNames{stn_neurons(k,1)}, '_', num2str(stn_neurons(k,2)), '.mat')); 
	y = signal.data;
    t = linspace(0,length(y), length(y));
    error = 0;
    try
        sp = load(strcat(times_file,'times_', originNames{stn_neurons(k,1)}, '_', num2str(stn_neurons(k,2)), '.mat'));
        neuronCount=max(sp.cluster_class(:,1));
        % median shape of the signals
        spikeTemplates=repmat(0,neuronCount,size(sp.spikes,2));
        for i=1:neuronCount
            spikeTemplates(i,:)= median(sp.spikes(find(sp.cluster_class(:,1)==i),:));
        end
        %%
        % removing the AP from all neurons
        y2=y;
        if size(sp.cluster_class,1)>0
            for i=1:size(sp.cluster_class,1)
                ti=sp.cluster_class(i,2)/1000; % time of AP
                ni=sp.cluster_class(i,1); % neuron of the AP
                if ni>0
                    if ti-sp.par.w_pre+1 >= 0 && ti+sp.par.w_post <= length(y2)
                        [~,ti]=min(abs(t-ti));
                        y2((ti-sp.par.w_pre+1):(ti+sp.par.w_post))=y2((ti-sp.par.w_pre+1):(ti+sp.par.w_post))-spikeTemplates(ni,:);
                    end
                end
            end
        end
        %%
        uexd_noAP = fill_artefact(x50, y2, artefact_UExd);
        error = 1;
    catch
        warning('No AP')
    end
    if error == 0
       uexd_noAP = fill_artefact(x50, x50, artefact_UExd);
    end
    save(strcat(save_file, originNames{stn_neurons(k,1)}, '_', num2str(stn_neurons(k,2)), '.mat'), 'uexd_noAP');
end