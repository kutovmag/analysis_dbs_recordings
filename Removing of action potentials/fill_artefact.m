% From the origin signal removes action potentials from all of the neurons
% the output signal will have the same length as have the origin signal
% (with artefacts) 
%
% Arguments:
% x - N array - original signal
% y - M array - signal wihtou action potentials and artifact
% artefact_UExd - 2 x number of artefact in this signal - in the first column 
%                 is the first sample of the artifact and in the second 
%                 column is the last sample of the artifact
%
% Returns: 
% uexd_noAP - N array - signal of the origin length without the 
%             action potentials 

function [uexd_noAP] = fill_artefact(x,y, artefact_Uexd)	
    artefact_count = 0;  
    position = 0; 
	uexd_noAP = zeros(1, length(x));
	for i = 1 : length(x)
        if sum(i <= artefact_Uexd(:,2) & i >= artefact_Uexd(:,1)) > 0
            artefact_count = artefact_count + 1;
            uexd_noAP(i) = x(i);
        else
            position = position + 1;
            uexd_noAP(i) = y(position);
        end
    end
end
