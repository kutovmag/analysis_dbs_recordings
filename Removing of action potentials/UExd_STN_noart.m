% from UExd(microelectrode recordings) signals (from STN) cuts the artfacts and due it shorten the signal

% It used text files with information:
% 	list_modified2.txt - names of the .d files with the data
% 	fs.mat - the sampling frequency for data
% 	STN_neurons.mat - matrix, in the first column is the order of the 
%   recordings which were identified in STN, and in the second column is 
%   the number of the electrode which was active in STN (recorded in STN)
% 	UExd_artefact.txt - list of the .csv files with the beginning and 
%   the end of the time where the artifact occurs.
% 
% Data from .mat files are loaded (electrodes that were active in STN). 
% Data is already preprocessed - comb filter (Loading.mat). And also, load 
% the .csv files with the time of artifacts. 
% 
% save_file - path, where we want to save the signal without artifacts

file = 'E:/FEL/Bakalarka/matlab/list_modified2.txt';
path = 'E:/FEL/Bakalarka/preprocessed_data_origin/';

f = fopen(file);
fileNames = textscan(f, '%s');
fclose(f);

fileNames = fileNames{1};
originNames = fileNames;
for l = 1 : length(originNames)
   originNames{l} = originNames{l}(1:end-2); 
end 
fileNames = strcat(fileNames,'_preprocessed.mat');

file = 'E:/FEL/Bakalarka/mat_files/fs.mat';
load(file);

stn_file ='E:/FEL/Bakalarka/mat_files/STN_neurons.mat';
load(stn_file);

artefact_file = 'E:/FEL/Bakalarka/art_annot/';

file = 'E:/FEL/Bakalarka/matlab/UExd_artefact.txt';
f = fopen(file);
artefactNames = textscan(f, '%s');
fclose(f);
artefactNames{1} = strcat(artefactNames{1},'_art.csv');

nbr_STN = length(stn_neurons(:,1));

save_file = 'E:/FEL/Bakalarka/UExd_noart/';

for k = 1 : length(stn_neurons(:,1))   
    load(strcat(path,fileNames{stn_neurons(k,1)}));
    Uexd = x50(6:end,:);
    x50 = Uexd(stn_neurons(k,2),:);
    
    f = fopen(strcat(artefact_file, artefactNames{1}{stn_neurons(k,1)}));
    artefacts = textscan(f, '%s %s %s');
    fclose(f);
   
    artefact_channel = zeros(length(artefacts{1}),5);
    for p = 1 : length(artefacts{1})
        if sum(artefacts{3}{p} == '1') == 1
           artefact_channel(p,1) = 1; 
        end
        if sum(artefacts{3}{p} == '2') == 1
           artefact_channel(p,2) = 1; 
        end
        if sum(artefacts{3}{p} == '3') == 1
           artefact_channel(p,3) = 1; 
        end
        if sum(artefacts{3}{p} == '4') == 1
           artefact_channel(p,4) = 1; 
        end
        if sum(artefacts{3}{p} == '5') == 1
           artefact_channel(p,5) = 1; 
        end
        if sum(artefacts{3}{p} == '0') == 1
           artefact_channel(p,:) = 1; 
        end

    end
    
    artefact = zeros(length(artefacts{1}),2);
    for j = 1 : length(artefacts{1})
       artefact(j,1) = str2num(artefacts{1}{j}(1:end-1));
       artefact(j,2) = str2num(artefacts{2}{j}(1:end-1));
    end
    
    artefact_idx = artefact .* fs;
    artefact_count = 0;  
    artefact_Uexd = artefact_idx(artefact_channel(:, stn_neurons(k,2)) == 1,:);
	position = 0; 
	uexd_noart = zeros(1, length(x50));
	for i = 1 : length(x50)
        if sum(i <= artefact_Uexd(:,2) & i >= artefact_Uexd(:,1)) > 0
            artefact_count = artefact_count + 1;
        else
            position = position + 1;
            uexd_noart(position) = x50(i);
        end
	end
	uexd_noart = uexd_noart(1:position);
	save(strcat(save_file, originNames{stn_neurons(k,1)}, '_', num2str(stn_neurons(k,2)), '.mat'), 'uexd_noart');
            
end
