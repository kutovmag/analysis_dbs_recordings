% run the WaveClus alghoritm 
%
% It used text files with information:
% 	list_modified2.txt - names of the .d files with the data
% 	STN_neurons.mat - matrix, in the first column is the order of the 
%   recordings which were identified in STN, and in the second column is 
%   the number of the electrode which was active in STN (recorded in STN)

% Data from .mat files are loaded (electrodes that were active in STN). 
% Data is already preprocessed - comb filter (Loading.mat) and without
% artfacts 
% 
% save_file - path, where we want to save the modified .mat file 
%             - the variable data is the signal and the variable sr 
%             is the sampling frequency

save_file = 'C:/Users/magda/Desktop/wave_clus-master/data/';

file = 'E:/FEL/Bakalarka/matlab/list_modified2.txt';
f = fopen(file);
fileNames = textscan(f, '%s');
fclose(f);
fileNames = fileNames{1};
stn_file ='E:/FEL/Bakalarka/mat_files/STN_neurons.mat';
load(stn_file);

clear param
param.stdmin = 5; % treshold for the detection
param.sr = 25000; % sampling frequency
param.detection = 'both'; % positive and negative action potentials
sr=25000;

for fi = 1 : length(stn_neurons(:,1))
    Name = fileNames{stn_neurons(fi,1)}(1:end-2);
    Name = strcat(Name, '_', num2str(stn_neurons(fi,2)));
    f = strcat('E:/FEL/Bakalarka/UExd_noart/', Name, '.mat');
    load(f)
    data = uexd_noart;
    save(strcat(save_file, Name),'data','sr')
    
    spikes_name = strcat(save_file, Name, '.mat');
    Get_spikes(spikes_name, 'par', param);

    Do_clustering(strcat(Name, '_spikes.mat'));
end



