% comb filter to filter 50 Hz and its multiplies
%
% Arguments:
% x - N array of the signal
% fs - sampling frequency
%
% Returns:
% x50 - N array of the same length as N but without 50Hz and its
%       multiplies

function [x50] = comb_filter(x, fs)
    f0 = 50; 
    D = fs / (2 * f0); % filter order

    b = zeros(1, D + 1); 
    b([1 end]) = 0.5;
    x50 = filter(b, 1, x); % FIR 
    
end