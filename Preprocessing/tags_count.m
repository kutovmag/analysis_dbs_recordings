% counts the number of required tags 
%
% Arguments:
% tags -  tags - tags read from x - a .D file 
% tagsToCount - string of the tags which we want to count in the signal
% 
% Returns:
% nbrTags - number of the required tags which is found in the signal

function [nbrTags] = tags_count(tags, tagsToCount)    
    nbrTags = 0;
    for tpi = 1 : length(tags.class)
        if ismember(tags.tabAbr{tags.class(tpi)+1}, tagsToCount)
           nbrTags = nbrTags + 1; 
        end
    end
end
