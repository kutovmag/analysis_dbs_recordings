% decimation of the sampling frequency by 100 with the function decimate
% (using FIR filter)
%
% Arguments:
% x - KxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
% fs - sampling frequency
%
% Returns:
% x_dec - KxM matrix of signals, same amout of rows as x but the number of
%         columns are lesser due to decimation of the sample frequency (100x)
% fs_dec - new sampling frequency after decimation, 2500

function [x_dec, fs_dec] = decimation(x, fs)
    %10x10
    fs_dec = fs / 100;
    % 25000 -> 250
    x_dec = zeros(length(x(:,1)), ceil(length(x(1,:)) / 100));
    for i = 1 : length(x(:,1))
       x_dec(i, :) = decimate(decimate(x(i,:), 10, 50, 'fir'), 10, 50, 'fir');
    end


end