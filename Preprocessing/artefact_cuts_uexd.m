% from the given signal cut the parts of the signal which occur after 
% the required tags, the cuttet part of the signal has given length fs *
% time
%
% modified artefact_cuts.m for a different way of detecting artefacts
%
% Arguments:
%  x - KxN matrix of signals (recorded in individual channels); signals
%      come in K rows, samples in N columns
% tags -  tags - tags read from a .D file 
% tagsToCut - name (or a cell array of names) of tags (their
%             2-character abbreviations) to cut.
% time - time interval which we want to cut after the tag occurs (int)
% quantity - nubmer of tags which occurs in the signal
% artefact - Mx2 matrix of indexes of the found artefact in the signal
%            first column is for beginning, second is for the end of the 
%            artefact
% fs - sampling frequency
%
% Returns:
% y - LxM matrix with the cuted parts of the signal where M is fs * time
%     and L is quantity - artefact_count 
% artefact_count - the number of the signal parts occured after the required
%                  tags but cannot be used because there is an artefact






function [y, artefact_count] = artefact_cuts_uexd(x, tags, tagsToCut, time, quantity, artefact, fs)
    artefact_count = 0;
	interval = int32(time * fs);
    y = zeros(quantity, int32(interval));
    position = 0;
	for tpi=1:length(tags.pos)
        tp = tags.pos(tpi);
        if ismember(tags.tabAbr{tags.class(tpi)+1},tagsToCut)
            % the beginnig of the signal we wanted is in the area where is
            % artefact
            if sum(tp <= artefact(:,2) & artefact(:,1) <= tp) > 0 
                artefact_count = artefact_count + 1;
            % the end of the signal we wanted is in the area where is
            % artefact
            elseif sum((tp + interval) <= artefact(:,2) & artefact(:,1) <= (tp + interval)) > 0 
                artefact_count = artefact_count + 1;
            else
                % tags occur at least the defaut time before the end of the
                % signal
                if (tp + interval - 1) <= length(x)
                    position = position + 1;
                    y(position,:) = x(tp : tp + interval - 1);
                end
            end
        end
	end
    y = y(1 : position, :);
end