% Loads the data from .d files and preprocesses them - filters with comb
% filter to surrpass the 50 Hz and its multiplies
% saves them as origin_name.d_preprocessed.mat files into the save_file
% uses list_modified2.txt is txt file with the names of the recordings

file = 'E:/FEL/Bakalarka/matlab/list_modified2.txt';
path = 'E:/FEL/Bakalarka/data/';

f = fopen(file);
fileNames = textscan(f, '%s');
fclose(f);
fileNames = fileNames{1};

save_file = '/data/home/kutovmag/preprocessed_data_origin/';
%%
for k = 1 : length(fileNames)
    [y,fs,channelNames,t,tags]=readDFile(strcat(path,fileNames{k})); % read a D file
    name = {'ME1','ME2','ME3','ME4','ME5', 'UE1d', 'UE2d', 'UE3d', 'UE4d', 'UE5d'};
    x = zeros(length(name), length(t));
    for i = 1 : length(name)
        s=strsplit(name{i},':');
        x(i,:) = getChannel(y, t, channelNames, s{1});
    end
    
    x50 = zeros(length(name), length(x(1,:)));
    for j = 1 : length(name)
       [x50(j,:)] = comb_filter(x(j,:), fs); 
    end

    % saving the file
    save_name = strcat(fileNames{k},'_preprocessed.mat');
    save(strcat(save_file, save_name), 'x50', 't', 'tags');
    
end