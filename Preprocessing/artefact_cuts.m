% from the given signal cuts the parts of the signal which occur after 
% the required tags, the cuttet part of the signal has given length fs *
% time
%
% Arguments:
% x - KxN matrix of signals (recorded in individual channels); signals
%     come in K rows, samples in N columns
% tags - tags read from x - a .D file 
% tagsToCut - name (or a cell array of names) of tags (their
%             2-character abbreviations) to cut.
% time - time interval which we want to cut after the tag occurs (int)
% quantity - nubmer of tags which occurs in the signal
% filter - if the data were filtered with the function filtfilt  - the
%          first and last second of the signal cannot be used for fowarding
%          processing
% treshold - minimum amplitude of the signal which is denoted as artefact
%
% Returns:
% y - LxM matrix with the cuted parts of the signal where M is fs (default) * time
%     and L is quantity minus the number of the signal parts occurd after the required
%     tags but cannot be used because there is an artefact

function [y] = artefact_cuts(x, tags, tagsToCut, time, quantity, filter, treshold)
    fs = 25000;
    % number of indexes in time interval
    idx_interval = int32(fs * time);
    y = zeros(length(x(:,1)) * quantity, idx_interval);
    start = 1;
    
    % total number of artifacts
    allArtifacts = 0;
    allFiltred = 0;
    
    for k = 1 : length(x(:,1))
        % positions of the artifact
        I = artefact_treshold(x(k,:), treshold);
        c = zeros(quantity , idx_interval);
        idx = 1;
        nbrArtifacts = 0;
        nbrFiltred = 0;
        
        for tpi = 1 : length(tags.pos)
            tp = tags.pos(tpi);
            if ismember(tags.tabAbr{tags.class(tpi)+1},tagsToCut)
                
                % if this tag is in the first or the last second we do not count it   
                if filter == true && (tp <= fs || tp >= (length(x(1,:)) - fs))
                    nbrFiltred  = nbrFiltred + 1;
                    continue
                end
                % if there is artifact we do not count this part of signal
                artefact_interval = sum(tp <= I & I <= (tp + idx_interval - 1));
                if  artefact_interval > 0
                    nbrArtifacts = nbrArtifacts + 1;
                    continue
                end
                if (tp + idx_interval - 1 <= length(x))
                    c(idx, :) = x(k, tp : tp + idx_interval - 1);
                    idx = idx + 1;
                else
                    nbrFiltred = nbrFiltred + 1;
                end
             end
        end
        
        % if there are artifacts we cut the nulls in the end of the matrix
        if nbrArtifacts ~= 0
            c(end - nbrArtifacts + 1 : end, :) = [];
            allArtifacts = allArtifacts + nbrArtifacts;
        end
        
        % if there are tags demaged during filtering we cut the nulls in the
        % end of the matrix
        if nbrFiltred ~= 0
            c(end - nbrFiltred + 1 : end, :) = [];
            allFiltred = allFiltred + nbrFiltred;
        end
        
        % save the signal to the resulting matrix
        y(start : start + length(c(:,1)) -1, :) = c;
        start = start + length(c(:,1)); 
    end
    
    % if there are artefacts or tags dameged during filtering
    % we cut the nulls in the end of the matrix
    y(end - allArtifacts + 1 : end, :) = []; 
    y(end - allFiltred + 1 : end, :) = []; 
end