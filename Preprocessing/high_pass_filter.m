% high pass filter of the signal x on the frequency f 
% with use of FIR filter - filter 
%
% Arguments:
% x - KxN matrix of signals (recorded in individual channels); signals
%     come in K rows, samples in N columns
% f0 - boundary for the high pass filter
% fs - sample frequency
%
% Returns:
% x_hp - KxN matrix of signals filtered with high pass filter (recorded 
%        in individual channels); signals come in K rows, samples in N columns
%


function [x_hp] = high_pass_filter(x, f0, fs)
    w0 = 2 * f0 / fs; 
    M = 70000; % filter order
    b_hp = fir1(M, w0, 'high'); 
    x_hp = x;
    for i = 1 : length(x(:,1))
        x_hp(i,:) = filter(b_hp, 1, x(i,:));
    end

end
