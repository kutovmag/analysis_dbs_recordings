% run both the statistics (bootstrap andt-test) for the recordings which
% came from electrode in STN in time domain 
% LFP signals
%
% It used text files with information:
% 	list_modified2.txt - names of the .d files with the data
% 	fs.mat - the sampling frequency for data
% 	STN_neurons.mat - matrix, in the first column is the order of the 
%   recordings which were identified in STN, and in the second column is 
%   the number of the electrode which was active in STN (recorded in STN)
%   LFP_artefacts.txt - txt file of the name of the signal and for its each
%   electrode is set the minimum amplitude for the artifact (how big
%   the sample have to have amplitude to be consider as artifact
% 
% Data from .mat files are loaded (electrodes that were active in STN). 
% Data is already preprocessed - comb filter (Loading.mat)
% 
% save_file - path, where we want to save the results from bootstrap and t-tests.

file = 'E:/FEL/Bakalarka/matlab/list_modified2_linux.txt';
path = 'E:/FEL/Bakalarka/preprocessed_data_origin/';

f = fopen(file);
fileNames = textscan(f, '%s');
fclose(f);

fileNames = fileNames{1};
fileNames = strcat(fileNames,'_preprocessed.mat');

file = 'E:/FEL/Bakalarka/mat_files/fs.mat';
load(file);

stn_file ='E:/FEL/Bakalarka/mat_files/STN_neurons.mat';
load(stn_file);

save_file = 'E:/FEL/Bakalarka/p_values/LFP/Time';

name = {'ME1','ME2','ME3','ME4','ME5'};

file = 'E:/FEL/Bakalarka/matlab/LFP_artefacts.txt';
f = fopen(file);
artefactsNames = textscan(f, '%s %s %s %s %s %s');
fclose(f);

artefacts = zeros(149, 5);
for i = 1 : 149
    artefacts(i,1) = str2num(artefactsNames{2}{i});
	artefacts(i,2) = str2num(artefactsNames{3}{i});
 	artefacts(i,3) = str2num(artefactsNames{4}{i});
	artefacts(i,4) = str2num(artefactsNames{5}{i});
	artefacts(i,5) = str2num(artefactsNames{6}{i});
end

nbr_STN = length(stn_neurons(:,1));
tagsN = {'N0','N1','N2','N3','N4','N5','N6','N7','N8','N9','N'};
tagsH = {'H0','H1','H2','H3','H4','H5','H6','H7','H8','H9','H'};
tagsF = {'F0','F1','F2','F3','F4','F5','F6','F7','F8','F9','F'};

b = 55;

bootstrap_pvalues = zeros(nbr_STN, 3);
bootstrap_boarders = zeros(nbr_STN, 6);
ttest_pvalues = zeros(nbr_STN, b * 3);


for k = 100 : 100 %length(stn_neurons(:,1))
    % loading the preprocessed .mat file - electrode that was active in STN
    load(strcat(path,fileNames{stn_neurons(k,1)}));
    lfp = x50(1:5,:);
    x50 = lfp(stn_neurons(k,2),:);
    
    % total number of required tags
    quantityN = tags_count(tags, tagsN);
    quantityH = tags_count(tags, tagsH);
    quantityF = tags_count(tags, tagsF);
   
    treshold = artefacts(stn_neurons(k,1),stn_neurons(k,2));
    % creates matrix just with the 550 ms intervals of the signals 
    % after required tag
    n = artefact_cuts(x50, tags, tagsN, 0.55, quantityN, true, treshold);
	h = artefact_cuts(x50, tags, tagsH, 0.55, quantityH, true, treshold);
	f = artefact_cuts(x50,tags, tagsF, 0.55, quantityF, true, treshold);

    % t - tests
    [ttest_pvalues(k,1:b), ~] = interval_ttests(n, h, b);  
    [ttest_pvalues(k,b + 1 : 2*b), ~] = interval_ttests(n, f, b);
    [ttest_pvalues(k, 2 * b + 1 : end)] = interval_ttests(f, h, b);
    
    % bootstrap
    N_dec = decimation(n, fs);
	H_dec = decimation(h, fs);
	F_dec = decimation(f, fs);

 	N_mean = mean(N_dec);
	H_mean = mean(H_dec);
    F_mean = mean(F_dec);

	diff_NH = N_mean - H_mean;
	diff_NF = N_mean - F_mean;
	diff_FH = F_mean - H_mean;

 	[real_maximum_NH, start_NH, end_NH] = max_area(diff_NH);
 	bootstrap_pvalues(k,1) = bootstrap_statistic(N_dec, H_dec, real_maximum_NH, 999);
 
 	[real_maximum_NF, start_NF, end_NF] = max_area(diff_NF);
 	bootstrap_pvalues(k,2) = bootstrap_statistic(N_dec, F_dec, real_maximum_NF, 999);

	[real_maximum_FH, start_FH, end_FH] = max_area(diff_FH);
	 bootstrap_pvalues(k,3) = bootstrap_statistic(F_dec, H_dec, real_maximum_FH, 999);
        
        
	bootstrap_boarders(k, 1) = start_NH;
	bootstrap_boarders(k, 2) = end_NH;
	bootstrap_boarders(k, 3) = start_NF;
	bootstrap_boarders(k, 4) = end_NF;
	bootstrap_boarders(k, 5) = start_FH;
	bootstrap_boarders(k, 6) = end_FH;
        
end
save(strcat(save_file, 'bootstrap_p_value'), 'bootstrap_pvalues');
save(strcat(save_file, 'ttest_p_value'), 'ttest_pvalues');
save(strcat(save_file, 'boarders'), 'bootstrap_boarders');
